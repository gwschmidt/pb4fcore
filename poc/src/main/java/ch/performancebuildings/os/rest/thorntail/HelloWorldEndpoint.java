package ch.performancebuildings.os.rest.thorntail;

import ch.performancebuildings.util.Str;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/hello")
public class HelloWorldEndpoint {

	@GET
	@Produces("text/plain")
	public Response doGet() {
		Str s = new Str();
		s.setValue("Hello from Thorntail!");
		return Response.ok(s.getValue()).build();
	}
}
