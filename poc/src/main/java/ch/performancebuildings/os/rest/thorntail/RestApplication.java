package ch.performancebuildings.os.rest.thorntail;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/thorntail/")
public class RestApplication extends Application {

}
