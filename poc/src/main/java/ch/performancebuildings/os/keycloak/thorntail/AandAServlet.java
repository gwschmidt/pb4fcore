package ch.performancebuildings.os.keycloak.thorntail;

import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AandA
 */
@WebServlet("/AandA")
public class AandAServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AandAServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		KeycloakClient client = new KeycloakClient();
		client.connect();
		response.getWriter()
			.append("You made it through KeyCloak")   
			.append("\nServed at: ").append(request.getContextPath())
			.append("\nUser: ").append(request.getRemoteUser());
		client.logToWriter(response.getWriter());
		response.getWriter()
			.append("\nUserData: ").append(client.getUserData(request.getRemoteUser()));  
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doGet(request, response);
	}
}
