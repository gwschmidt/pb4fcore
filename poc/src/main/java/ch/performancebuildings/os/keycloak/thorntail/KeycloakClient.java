package ch.performancebuildings.os.keycloak.thorntail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import javax.json.Json;
import javax.json.JsonObject;

public class KeycloakClient {

	//private static final String PASSWORD = "qwertz";
	//private static final String USERNAME = "gws";
	//private static final String REALM = "PB-Application";
	//private static final String HOST = "http://localhost:9090";
	private static final String PASSWORD = "qwertz";
	private static final String USERNAME = "test_admin";
	private static final String REALM = "test";
	private static final String HOST = "http://localhost:9080";
	
	private String access_token;
	private int expires_in;
	private int refresh_expires_in;
	private String refresh_token;
	private String token_type;
	private int not_before_policy;
	private String session_state;
	private String scope;
	
	public boolean connect() {
		try {
			String connectUrl = String.format("%s/auth/realms/%s/protocol/openid-connect/token", 
					HOST, REALM);
			URL keyCloakAdminUrl = new URL(connectUrl);
			HttpURLConnection connection = (HttpURLConnection)keyCloakAdminUrl.openConnection();
		    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
		    connection.setRequestProperty("Accept", "application/json");
			connection.setRequestMethod("POST");
		    connection.setDoOutput(true);
		    connection.setUseCaches(false);
		    OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
		    osw.write("username=");
		    osw.write(USERNAME);
		    osw.write("&password=");
		    osw.write(PASSWORD);
		    osw.write("&grant_type=");
		    osw.write("password");
		    osw.write("&client_id=");
		    osw.write("admin-cli");
			osw.close();
			
		    //send request
		    int responseCode = connection.getResponseCode();
			System.out.println(String.format("Response code for call %s: %s", keyCloakAdminUrl, responseCode));
			
			if (responseCode == 200) {
				JsonObject obj = Json.createReader(connection.getInputStream()).readObject();
				access_token = obj.getString("access_token");
				expires_in = obj.getInt("expires_in");
				refresh_expires_in = obj.getInt("refresh_expires_in");
				refresh_token = obj.getString("refresh_token");
				token_type = obj.getString("token_type");
				not_before_policy = obj.getInt("not-before-policy");
				session_state = obj.getString("session_state");
				scope = obj.getString("scope");
				return true;
			}
			else {
				return false;
			}
		} catch (IOException e) {
			System.out.append(e.getMessage());
		}
		return true;
	}
	
	public String getUserData(String login) {
		try {
			String userUrl = String.format("%s/auth/admin/realms/%s/users/", 
					HOST, REALM);
			URL keyCloakUserUrl = new URL(new URL(userUrl), login);
			HttpURLConnection connection = (HttpURLConnection)keyCloakUserUrl.openConnection();
			connection.setRequestProperty("Authorization", "Bearer " + access_token);
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestMethod("GET");
			
		    //send request
		    int responseCode = connection.getResponseCode();
			System.out.println(String.format("Response code for call %s: %s", keyCloakUserUrl, responseCode));
			
			if (responseCode == 200) {
				StringBuilder textBuilder = new StringBuilder();
			    try (Reader reader = new BufferedReader(new InputStreamReader
			    	      (connection.getInputStream(), Charset.forName(StandardCharsets.UTF_8.name())))) {
			        int c;
			        while ((c = reader.read()) != -1) {
			            textBuilder.append((char) c);
			        }
			    }
				return textBuilder.toString();
			}
			else {
				return null;
			}
		} catch (IOException e) {
			System.out.append(e.getMessage());
		}
		return null;
	}
	
	public void logToWriter(Writer writer) {
		try {
			writer
				.append("\naccess_token: ").append(access_token)   
				.append("\nexpires_in: ").append(Integer.toString(expires_in)) 
				.append("\nrefresh_expires_in: ").append(Integer.toString(refresh_expires_in)) 
				.append("\nrefresh_token: ").append(refresh_token)  
				.append("\ntoken_type: ").append(token_type) 
				.append("\nnot_before_policy: ").append(Integer.toString(not_before_policy))  
				.append("\nsession_state: ").append(session_state)  
				.append("\nscope: ").append(scope);
		} catch (IOException e) {
			System.out.append(e.getMessage());
		}
	}
	
	@Override
	public String toString() {
		return "KeyCloakClient [access_token=" + access_token + ", expires_in=" + expires_in + ", refresh_expires_in="
				+ refresh_expires_in + ", refresh_token=" + refresh_token + ", token_type=" + token_type
				+ ", not_before_policy=" + not_before_policy + ", session_state=" + session_state + ", scope=" + scope
				+ "]";
	}
}
