package ch.performancebuildings.os.vaadin.thorntail;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.templatemodel.TemplateModel;

import java.time.Instant;

/**
 * A Designer generated component for the test-design template.
 *
 * Designer will add and remove fields with @Id mappings but
 * does not overwrite or otherwise change this file.
 *
 * You have to include polymer dependencies in the pom.xml. Generating
 * UIs with the designer comes with a significant cost (> 100$ per month)
 * and might not be useful for open source projects
 */
@Route
@Tag("test-design")
@JsModule("./test-design.js")
public class TestDesign extends PolymerTemplate<TestDesign.TestDesignModel> {

    @Id("footer1")
    private Label footer1;
    @Id("header1")
    private Label header1;
    @Id("button")
    private Button button;

    /**
     * Creates a new TestDesign.
     */
    public TestDesign() {
        // You can initialise any data required for the connected UI components here.
        header1.setText("Header 1");
        footer1.setText("Footer 1");
        /*button.addClickListener(new ComponentEventListener<ClickEvent<Button>>() {

            @Override
            public void onComponentEvent(ClickEvent<Button> buttonClickEvent) {
                header1.setText("Header 1: clicked");
                footer1.setText("Footer 1: " + Instant.now());
            }
        }); */
        button.addClickListener(onComponentEvent -> {
            header1.setText("Header 1: clicked");
            footer1.setText("Footer 1: " + Instant.now());
        });
    }

    /**
     * This model binds properties between TestDesign and test-design
     */
    public interface TestDesignModel extends TemplateModel {
        // Add setters and getters for template properties here.
    }
}
