package ch.performancebuildings.os;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;

/**
 * The view contains a button and a click listener. It's accessible on the default route
 *  /myui
 */
@Route
@PWA(name = "My UI", shortName = "My UI")
public class MyUI extends VerticalLayout {

    public MyUI() {
        Button button = new Button("Click me",
                event -> Notification.show("Clicked!"));
        add(button);
    }
}
