package ch.performancebuildings.os.settings;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Base class of a settings bundle.
 */
public abstract class PB4Settings {
	
	public static final String TYPE_KEY = "type";
	public static final String ID_KEY = "_id";
	private static final String VERSION_KEY = "version";
	
	protected Document document;

	public abstract int getVersion();

	public String getInstanceIdentifer() {
		return document.getString(ID_KEY);
	}
	
	public String getType() {
		return document.getString(TYPE_KEY);
	}
	
	protected void init(String instanceIdentifer) {
		if (document == null) {
			document = new Document(ID_KEY, instanceIdentifer);
			document.append(VERSION_KEY, getVersion());
			document.append(TYPE_KEY, this.getClass().getName());
		} else if (!document.get(ID_KEY).equals(instanceIdentifer)) {
			throw new SettingsException(String.format("id %s expected but %s passed", document.get(ID_KEY), instanceIdentifer),
					SettingsException.Code.ID_MISSMATCH);
		}
	}
	
	protected void init(Document document) {
		assert document != null;
		if (this.document != null) {
			throw new SettingsException("document already set", SettingsException.Code.DUPLICATE_DOCUMENT_ASSIGNMENT);
		}
		this.document = document;
		if (document.getInteger(VERSION_KEY) != getVersion()) {
			throw new SettingsException("settings version missmatch", SettingsException.Code.VERSION_MISSMATCH);
		}
	}

	void write(MongoCollection<Document> collection) {
		BasicDBObject obj = new BasicDBObject(ID_KEY, document.get(ID_KEY));
		FindIterable<Document> iterator = collection.find(obj);
		Document oldDocument = iterator.first();
		if (oldDocument == null) {
			collection.insertOne(document);
		}
		else {
			collection.replaceOne(obj, document);
		}
	}

	void writeDefaultValues(MongoCollection<Document> collection) {
		for (Method getter : this.getClass().getMethods()) {
			String getterName = getter.getName();
			if (getterName.startsWith("get") &&
					!getterName.equals("getVersion") &&
					!getterName.equals("getType") &&
					!getterName.equals("getInstanceIdentifier") &&
					!getterName.equals("getClass")) {
				try {
					Object obj = getter.invoke(this);
					String setterName = "s" + getterName.substring(1);
					Method setter = this.getClass().getMethod(setterName, getter.getReturnType());
					setter.invoke(this, obj);
				} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
					e.printStackTrace();
				}
			}
		}
		write(collection);
	}
}
