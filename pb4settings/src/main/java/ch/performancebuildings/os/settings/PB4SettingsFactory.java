package ch.performancebuildings.os.settings;

import ch.performancebuildings.os.settings.SettingsException.Code;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.lang.reflect.InvocationTargetException;

/**
 * Read and write settings of a PB4F logical unit called an instance. A logical unit is identified by an
 * instanceIdentifier.
 */
public class PB4SettingsFactory {
 
	private MongoClient client;
	private MongoCollection<Document> collection;

	private final static String paramHost = "PB4F_SETTINGS_HOST";
	private final static String paramPort = "PB4F_SETTINGS_PORT";

	private final static String defaultHost = "localhost";
	private final static int defaultPort = 27017;
	private final static String settingsDatabase = "pbconfigs";
	private final static String settingsCollection = "config4";

	public PB4SettingsFactory() {
		Integer port = null;
		// environment variables
		String host = System.getenv(paramHost);
		String port1 = System.getenv(paramPort);
		if (port1 != null) {
			port = safeParseUnsignedInt(port1);
		}
		// vm variables -D...
		if (host == null) {
			host = System.getProperty(paramHost);
		}
		if (port == null) {
			port = Integer.getInteger(paramPort);
		}
		// default values
		if (host == null) {
			host = defaultHost;
		}
		if (port == null) {
			port = defaultPort;
		}
		client = new MongoClient(host, port);
		MongoDatabase configsDb = client.getDatabase(settingsDatabase);
		collection = configsDb.getCollection(settingsCollection);
	}

	private Integer safeParseUnsignedInt(String value) {
		try {
			return Integer.parseUnsignedInt(value);
		} catch (Exception e) {
			return null; // behaves like Integer.getInteger
		}
	}
	
	public void close() {
		//client.close();
	}

	public <S extends PB4Settings> void writeSettings(String instanceIdentifer, S settings) {
		settings.init(instanceIdentifer);
		settings.write(collection);
	}

	public <S extends PB4Settings> void writeSettingsWithDefaultValues(String instanceIdentifer, S settings) {
		settings.init(instanceIdentifer);
		settings.writeDefaultValues(collection);
	}
	
	public <S extends PB4Settings> S readSettings(String instanceIdentifer, Class<S> clazz) {
		S result;
		try {
			result = clazz.getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			throw new SettingsException(e.getMessage(), Code.SETTINGS_CREATE_ERROR); 
		}
		FindIterable<Document> iterator = collection.find(new BasicDBObject(PB4Settings.ID_KEY, instanceIdentifer));
		Document document = iterator.first();
		if (document != null) {
			result.init(document);
			return result;
		}
		else {
			result.init(instanceIdentifer);
			return result;
		}
	}
	
	public <S extends PB4Settings> S readSettings(String instanceIdentifer) {
		FindIterable<Document> iterator = collection.find(new BasicDBObject(PB4Settings.ID_KEY, instanceIdentifer));
		Document document = iterator.first();
		if (document != null) {
			try {
				Class<?> clazz = Class.forName(document.getString(PB4Settings.TYPE_KEY));
				@SuppressWarnings("unchecked")
				S result = (S)clazz.getConstructor().newInstance();
				result.init(document);
				return result;
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException | ClassNotFoundException e) {
				throw new SettingsException(e.getMessage(), Code.SETTINGS_CREATE_ERROR); 
			}
		}
		else {
			throw new SettingsException("no such instance identifier: " + instanceIdentifer, Code.NO_SUCH_INSTANCE_IDENTIFIER); 
		}
	}
}
