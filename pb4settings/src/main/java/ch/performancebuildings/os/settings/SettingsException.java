package ch.performancebuildings.os.settings;

/**
 * Exception thrown by settings failures.
 */
public class SettingsException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public enum Code {
		VERSION_MISSMATCH,
		SETTINGS_CREATE_ERROR,
		NO_SUCH_INSTANCE_IDENTIFIER,
		ID_MISSMATCH,
		DUPLICATE_DOCUMENT_ASSIGNMENT
	}
	
	private Code code;
	
	public SettingsException(String message, Code code) {
		super(message);
		this.code = code;
	}

	public Code getCode() {
		return code;
	}
}
