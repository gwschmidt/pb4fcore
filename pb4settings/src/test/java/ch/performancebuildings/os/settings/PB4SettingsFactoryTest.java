package ch.performancebuildings.os.settings;

import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

public class PB4SettingsFactoryTest {

    @Test
    public void writeSettings() {
        PB4SettingsFactory factory = new PB4SettingsFactory();
        try {
            // not in DB
            String id = UUID.randomUUID().toString();
            SampleSettings settings = new SampleSettings();
            factory.writeSettings(id, settings);
        }
        finally {
            factory.close();
        }
    }

    @Test
    public void readSettings() {
        PB4SettingsFactory factory = new PB4SettingsFactory();
        try {
            // not from DB
            String id = UUID.randomUUID().toString();
            SampleSettings s = factory.readSettings(id, SampleSettings.class);
            assertEquals(1, s.getIntProperty());
            assertNull(s.getNullableIntProperty());
            assertEquals(1, s.getVersion());
            assertEquals(id, s.getInstanceIdentifer());
            assertEquals("ch.performancebuildings.os.settings.SampleSettings", s.getType());
        }
        finally {
            factory.close();
        }
    }

    @Test
    public void testReadSettings() {
        PB4SettingsFactory factory = new PB4SettingsFactory();
        try {
            // not in DB
            String id = UUID.randomUUID().toString();
            SampleSettings settings = factory.readSettings(id, SampleSettings.class);
            settings.setIntProperty(5);
            settings.setNullableIntProperty(6);
            factory.writeSettings(id, settings);
            PB4Settings s = factory.readSettings(id);
            assertTrue(s instanceof SampleSettings);
            settings = (SampleSettings)s;
            assertEquals(5, settings.getIntProperty());
            assertEquals(6, (long)settings.getNullableIntProperty());
            assertEquals(1, settings.getVersion());
            assertEquals(id, settings.getInstanceIdentifer());
            assertEquals("ch.performancebuildings.os.settings.SampleSettings", settings.getType());
        }
        finally {
            factory.close();
        }
    }
}