package ch.performancebuildings.os.settings;

public class SampleSettings extends PB4Settings {

	private static final String INT_PROPERTY_KEY = "IntProperty";
	private static final int INT_PROPERTY_DEFAULT = 1;
	private static final String NULLABLE_INT_PROPERTY_KEY = "NullableIntProperty";
	
	@Override
	public int getVersion() {
		return 1;
	}
	
	public int getIntProperty() {
		return document.getInteger(INT_PROPERTY_KEY, INT_PROPERTY_DEFAULT);
	}
	
	public void setIntProperty(int value) {
		document.putIfAbsent(INT_PROPERTY_KEY, value);
	}
	
	public Integer getNullableIntProperty() {
		return document.getInteger(NULLABLE_INT_PROPERTY_KEY);
	}
	
	public void setNullableIntProperty(Integer value) {
		document.putIfAbsent(NULLABLE_INT_PROPERTY_KEY, value);
	}
}
